-- Lua-Stat - A lightweight Lua profiler.

local gettime, epoch, fprec
local Stat = require 'struct' 'Stat'

--[[
	Initialize a new stat object.
	
	Parameters:
	-	name: the name of the new stat.
	-	prev: the stat's parent.  If not present, this is the root stat.
	-	tab: the table to store the stat in.  When child stats are
			created, the 'stat' key in this table will be updated with
			the current stat.
--]]
function Stat:__init(name, prev, tab)
	self.name = name
	self.stime = gettime() - epoch
	self.prev = prev and prev + self
	self.depth = prev and prev.depth + 2 or 0
	self.tab = prev and prev.tab or tab
	self.tab['stat'] = self
end

--[[
	Close a stat object.
	
	Marks the stat as finished, and calculates the time spent.
	When the root stat is closed, `Stat:Init()` can safely be called again.
	
	Returns:
	-	the parent stat.  If this is the root stat, returns itself.
--]]
function Stat:Exit()
	self.etime = gettime() - epoch
	self.total = self.etime - self.stime
	local prev = self.prev or self
	if self.tab['stat'] == self then
		self.tab['stat'] = prev
	end
	return prev
end

--[[
	The main interaction with the stat object.
	
	Parameters:
	-	name: the name of the new stat.  If present, creates a new child stat.
			otherwise, closes this stat and returns the parent.
	
	Returns:
	-	the new child stat, if name was present; the parent stat otherwise
--]]
function Stat:__call(name)
	if name then
		return Stat(name, self, self.tab)
	else
		return self:Exit()
	end
end

--[[
	Generate a textual representation of a stat chain.
	
	Parameters:
	-	depth: the number of levels to print.  Nil or a negative number will
			print all levels.
	Returns:
	-	the generated string.
--]]
function Stat:Print(depth)
	depth = depth or -1
	if depth == 0 then return "" end
	local ind = self.depth > 0 and string.rep(' ', self.depth) .. '> ' or ""
	local out = ind .. tostring(self) .. '\n'
	for i,v in ipairs(self) do
		out = out .. v:Print(depth - 1)
	end
	return out
end

-- Pretty print a stat.
function Stat:__tostring()
	local s,e = self.stime, self.etime or -1
	local t = self.total or -1
	local tf = '%'..fprec..'fms'
	local fmt = '%s | start: '..tf..', end: '..tf..', total: '..tf..'(%.3fs)'
	return fmt:format(self.name, s, e, t, t/1000)
end

-- Internal helper for stat linking.
function Stat:__add(new)
	assert(Stat <= new)
	if not self.stop then
		table.insert(self, new)
	end
	return self
end

--[[
	Initialize the main stat.
	
	This function shall only be called when there is no running stat.
	
	Parameters:
	-	provider: the time provider to use.  Can be 'SDL', 'posix', or nil.
	-	tab: the table to store the the stat in.  Defaults to _G.
	
	Returns:
	-	the new stat object.
--]]
function Stat:Init(provider, tab)
	tab = tab or _G
	if provider == 'SDL' then
		local SDL = require 'SDL'
		gettime = SDL.getTicks
		fprec = '.0' -- 1ms precision
		SDL.delay(0) -- starts ticking, irrespective of SDL's state
	elseif provider == 'posix' then
		local time = require 'posix.time'
		local clock = time['CLOCK_MONOTONIC']
		fprec = '.2' -- 0.01ms precision
		gettime = function()
			local t = time.clock_gettime(clock)
			local us = t.tv_nsec // 10000 -- 1ns to 10us
			return t.tv_sec * 1000 + us / 100 -- convert to ms
		end
	else -- Fallback to os.time
		gettime = function () return os.time() / 1000 end
		fprec = '.0' -- 1000ms precision
	end
	epoch = gettime()
	return Stat('main', nil, tab)
end

return Stat
