#!/usr/bin/env lua

local main_stat = dofile('init.lua'):Init(arg[1])

stat 'setup'

function do_a_thing()
	stat 'Thing'
	for i = 1, 1<<12 do
		io.stdout:write ''
	end
	io.stdout:flush()
	stat()
end

stat ()

stat 'run'

for i = 1, 1<<5 do
	do_a_thing()
end

stat()

print(stat():Print())
print(dofile 'analyzer.lua' (stat))
