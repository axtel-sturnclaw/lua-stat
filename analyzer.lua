-- Stat analyser v0.1

local function recurse(stat, times)
	for _, stat in ipairs(stat) do
		local t = times[stat.name] or {}
		table.insert(t, stat.total or -1)
		times[stat.name] = t
		recurse(stat, times)
	end
end

local function average(times)
	for name, t in pairs(times) do
		table.sort(t)
		local acc = 0
		for _, i in ipairs(t) do acc = acc + i end
		times[name] = {
			name = name,
			min = t[1],
			max = t[#t],
			avg = acc / #t
		}
	end
end

local function list(times, sort)
	local t, f = {}, function(a, b) return a[sort] > b[sort] end
	for _, v in pairs(times) do table.insert(t, v) end
	table.sort(t, f)

	local l = ""
	for _, v in ipairs(t) do
		l = l .. string.format("> %s | avg: %.2f, min: %.2f, max: %.2f",
			v.name, v.avg, v.min, v.max) .. "\n"
	end

	return l
end

local function analyze(stat, sort)
	local times = {}

	recurse({stat}, times)
	average(times)

	return list(times, sort or 'avg')
end

return analyze
