# Lua Stat

A simple module to gather debug timings for Lua applications. Lua Stat has
several backends for gathering timing information, each with its own level of
precision.

## Dependencies:

For a basic install, Lua Stat depends upon [lstruct](https://gitlab.com/axtel-sturnclaw/lstruct).

If you wish to use the SDL backend, you will need [Lua-SDL2](https://github.com/tangent128/luasdl2)
installed.  If you wish to use the POSIX backend, you will need
[luaposix](https://github.com/luaposix/luaposix).

### Usage:

Lua Stat is loaded as a module, via `require`.	This returns the `Stat` struct,
which must be initialized via a call to `Stat:Init()`.	This selects a backend,
and starts the main stat, which by default is placed in the global namespace
as `stat`.

A call to an initialized `stat` does one of two things:
* If called with a string as an argument, it creates a child stat, and places this
new stat in the global namespace at the same location.
* If called with no arguments, it finishes the current stat and, if it has a
parent stat, replaces itself in the global namespace with the parent stat.

Here's some example code:

``` lua
local Stat = require 'stat' -- Load the Stat struct
local main = Stat:Init() -- Creates the 'main' stat.
-- _G.stat now refers to the main stat

stat 'sub-stat' -- creates a child stat of the main state
-- _G.stat now refers to our newly-created sub-stat

-- do stuff...

stat() -- closes the child stat
-- _G.stat is now back to the main stat

-- more stuff...

stat() -- closes the main stat

-- generates a string representation of the stats and the time taken
local stats = main:Print()
print(stats)
```

### The Backends:

#### [Standard Lua](http://lua.org)
When no backend is specified, Lua Stat uses plain Lua `os.time`. This usually
provides a resolution in seconds, and as such is not really useful for more
than a fallback.

```lua
Stat:Init()
```

#### [SDL2](https://github.com/Tangent128/LuaSDL2)
Lua Stat can use Lua-SDL2 for it's timing information. This provides a
resolution in milliseconds and will work almost anywhere.

```lua
Stat:Init('SDL')
```

#### [luaposix](https://github.com/luaposix/luaposix)
Lua Stat can also use luaposix for it's timing information. This, while
capable of 1ns precision, is limited to 0.01ms as Lua's overhead renders
any smaller resolution useless.

```lua
Stat:Init('posix')
```
